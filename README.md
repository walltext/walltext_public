# WallNotifyMe

Android Studio App to manage a LED Matrix connected to arduino via Bluetooth.
In the repo you can find both the source code of the app and the source code to upload to arduino. 
The arduino sketch is provided by an example from the MD_parola library.