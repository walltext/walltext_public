package com.example.mala.textwall;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;

/**
 *
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

public class NotificationListenerService extends android.service.notification.NotificationListenerService {

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    private long lastPostTime = 0;

    @Override
    public void onNotificationPosted(StatusBarNotification sbn){

        long newTime = sbn.getPostTime();

        if (newTime - lastPostTime > 5000) {

            lastPostTime = newTime;

            Notification n = sbn.getNotification();
            String ncat = sbn.getNotification().category;
            Bundle e = n.extras;
            String etitle = e.getString("android.title");
            String emessage = e.getString("android.text");

            if(!etitle.isEmpty() && !emessage.isEmpty() && !TextUtils.equals(emessage, "android.text") && !TextUtils.equals(etitle, "android.title")) {
                Intent intent = new Intent("com.github.chagall.notificationlistenerexample");
                Bundle b = new Bundle();
                b.putString("Category", ncat);
                b.putString("Title", etitle);
                b.putString("Message", emessage);
                intent.putExtras(b);
                sendBroadcast(intent);
            }
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn){
        Intent intent = new Intent("com.github.chagall.notificationlistenerexample");
        sendBroadcast(intent);
    }

}
