package com.example.mala.textwall;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.net.Uri;
import android.view.View;

public class InstructionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
    }

    public void openWeb(View v){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://bitbucket.org/walltext/walltext_public/overview"));
        startActivity(intent);
    }

    public void backHome(View v){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
